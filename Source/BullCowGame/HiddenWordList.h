#pragma once

#include "CoreMinimal.h"

const TArray<FString> Words =
{
    TEXT("link"),
    TEXT("juice"),
    TEXT("table"),
    TEXT("phone"),
    TEXT("terror"),
    TEXT("computer"),
    TEXT("dog"),
    TEXT("bark"),
    TEXT("cat"),
    TEXT("bear"),
    TEXT("addict"),
    TEXT("helicopter"),
    TEXT("light"),
    TEXT("spider"),
    TEXT("set"),
    TEXT("meat"),
    TEXT("milk"),
    TEXT("sun"),
    TEXT("brick"),
    TEXT("step")
};